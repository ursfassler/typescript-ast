import {
  Node,
  SourceFile,
  Identifier,
  StringLiteral,
  InterfaceDeclaration,
  MethodSignature,
  FunctionDeclaration,
  MethodDeclaration,
  ClassDeclaration,
  ConstructorDeclaration,
  TypeReferenceNode,
  CallExpression,
  PropertyAccessExpression,
  ThisExpression,
  SyntaxKind,
  PropertyDeclaration,
  VariableDeclaration,
  GetAccessorDeclaration,
  SetAccessorDeclaration,
} from "ts-morph";


export interface Visitor {
  begin(node: Node): void;
  end(node: Node): void;

  visitSourceFile(node: SourceFile): void;
  visitIdentifier(node: Identifier): void;
  visitStringLiteral(node: StringLiteral): void;
  visitInterfaceDeclaration(node: InterfaceDeclaration): void;
  visitMethodSignature(node: MethodSignature): void;
  visitClassDeclaration(node: ClassDeclaration): void;
  visitConstructor(node: ConstructorDeclaration): void;
  visitMethodDeclaration(node: MethodDeclaration): void;
  visitGetAccessorDeclaration(node: GetAccessorDeclaration): void;
  visitSetAccessorDeclaration(node: SetAccessorDeclaration): void;
  visitPropertyDeclaration(node: PropertyDeclaration): void;
  visitFunctionDeclaration(node: FunctionDeclaration): void;
  visitVariableDeclaration(node: VariableDeclaration): void;
  visitTypeReference(node: TypeReferenceNode): void;
  visitCallExpression(node: CallExpression): void;
  visitPropertyAccessExpression(node: PropertyAccessExpression): void;
  visitThisKeyword(node: ThisExpression): void;

  visit(node: Node): void;
}


export function dispatch(node: Node, visitor: Visitor): void {
  switch (node.getKind()) {
    case SyntaxKind.SourceFile:
      visitor.visitSourceFile(node as SourceFile);
      break;

    case SyntaxKind.Identifier:
      visitor.visitIdentifier(node as Identifier);
      break;

    case SyntaxKind.StringLiteral:
      visitor.visitStringLiteral(node as StringLiteral);
      break;

    case SyntaxKind.InterfaceDeclaration:
      visitor.visitInterfaceDeclaration(node as InterfaceDeclaration);
      break;

    case SyntaxKind.MethodSignature:
      visitor.visitMethodSignature(node as MethodSignature);
      break;

    case SyntaxKind.ClassDeclaration:
      visitor.visitClassDeclaration(node as ClassDeclaration);
      break;

    case SyntaxKind.Constructor:
      visitor.visitConstructor(node as ConstructorDeclaration);
      break;

    case SyntaxKind.GetAccessor:
      visitor.visitGetAccessorDeclaration(node as GetAccessorDeclaration);
      break;

    case SyntaxKind.SetAccessor:
      visitor.visitSetAccessorDeclaration(node as SetAccessorDeclaration);
      break;

    case SyntaxKind.MethodDeclaration:
      visitor.visitMethodDeclaration(node as MethodDeclaration);
      break;

    case SyntaxKind.PropertyDeclaration:
      visitor.visitPropertyDeclaration(node as PropertyDeclaration);
      break;

    case SyntaxKind.FunctionDeclaration:
      visitor.visitFunctionDeclaration(node as FunctionDeclaration);
      break;

    case SyntaxKind.VariableDeclaration:
      visitor.visitVariableDeclaration(node as VariableDeclaration);
      break;

    case SyntaxKind.TypeReference:
      visitor.visitTypeReference(node as TypeReferenceNode);
      break;

    case SyntaxKind.CallExpression:
      visitor.visitCallExpression(node as CallExpression);
      break;

    case SyntaxKind.PropertyAccessExpression:
      visitor.visitPropertyAccessExpression(node as PropertyAccessExpression);
      break;

    case SyntaxKind.ThisKeyword:
      visitor.visitThisKeyword(node as ThisExpression);
      break;

    default:
      visitor.visit(node);
      break;
  }
}

