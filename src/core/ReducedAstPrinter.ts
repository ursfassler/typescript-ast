import path from "path";
import {
  Node,
  SourceFile,
  Identifier,
  StringLiteral,
  InterfaceDeclaration,
  MethodSignature,
  FunctionDeclaration,
  MethodDeclaration,
  ClassDeclaration,
  ConstructorDeclaration,
  TypeReferenceNode,
  CallExpression,
  PropertyAccessExpression,
  ThisExpression,
  PropertyDeclaration,
  SyntaxKind,
  VariableDeclaration,
  GetAccessorDeclaration,
  SetAccessorDeclaration,
} from "ts-morph";
import { Element, Type } from "./Element";
import { Visitor } from "./Visitor";


export class ReducedAstPrinter implements Visitor {
  private elements: Array<Element> = [];
  private nodeToElement: Map<Node, Element> = new Map();
  private references: Map<Element, Array<Node>> = new Map();
  private packageToModuleCode: Map<Element, Element> = new Map();

  constructor(private root: Element, private rootDirectory: string) {
  }

  beginProject(): void {
    this.elements.push(this.root);
  }

  endProject(): void {
    this.elements.pop();

    this.resolveReferences();
    this.addNonEmptyModuleCode();
  }

  begin(node: Node): void {
  }

  end(node: Node): void {
    if (node.getKind() == SyntaxKind.SourceFile) {
      while (this.currentElement() != this.root) {
        this.pop();
      }
    } else
      if ((this.elements.length > 0)) {
        const current = this.currentElement();
        const byNode = this.findElement(node);
        if (current == byNode) {
          this.pop();
        }
      }
  }

  visitSourceFile(node: SourceFile): void {
    const moduleCode = new Element(Type.Function, "__init__");
    this.registerElement(node, moduleCode);

    const full = node.getFilePath();
    const relativePath = path.relative(this.rootDirectory, full);
    const extname = path.extname(relativePath);
    const namespaced = relativePath.substring(0, relativePath.length - extname.length);
    const namespace = namespaced.split(path.sep);

    let current: Element = this.currentElement();
    for (let part of namespace) {
      let maybeChild: Element | undefined = current.children.find(x => x.name === part);

      if (maybeChild) {
        this.elements.push(maybeChild);
        current = maybeChild;
      } else {
        const element = new Element(Type.Package, part);
        // we don't register namespaces or modules
        this.push(element);
        current = element;
      }
    }

    this.packageToModuleCode.set(current, moduleCode);
  }

  visitIdentifier(node: Identifier): void {
    if (this.isImportIdentifier(node)) {
      return;
    }

    const declaration = this.findDeclarationNode(node);

    const currentElement = this.currentElement();
    const from = this.getModuleCodeIfPackage(currentElement) ?? currentElement;

    const definitions: Node[] = this.getDefinitionsNodes(node);
    for (const definition of definitions) {
      const referenceToItself = definition === declaration;
      if (referenceToItself) {
        continue;
      }

      const isSystem = definition.getSourceFile().isInNodeModules();
      if (isSystem) {
        continue;
      }

      //TODO filter out references to nodes we don't emit (e.g. parameter)
      this.addReference(from, definition);
    }
  }

  private getDefinitionsNodes(node: Identifier): Node[] {
    try {
      return node.getDefinitionNodes();
    } catch (error) {
      //TODO why is this exception thrown?
      return [];
    }
  }

  private getModuleCodeIfPackage(element: Element): Element | undefined {
    return this.packageToModuleCode.get(element);
  }

  private isImportIdentifier(node: Identifier): boolean {
    return this.parentIs(node, [
      SyntaxKind.ImportSpecifier,
      SyntaxKind.NamespaceImport,
    ]);
  }

  private findDeclarationNode(node: Identifier): Node | null {
    return this.findAncestorOfKind(node, [
      SyntaxKind.ClassDeclaration,
      SyntaxKind.InterfaceDeclaration,
      SyntaxKind.GetAccessor,
      SyntaxKind.SetAccessor,
      SyntaxKind.MethodDeclaration,
      SyntaxKind.PropertyDeclaration,
      SyntaxKind.FunctionDeclaration,
      SyntaxKind.VariableDeclaration,
    ]);
  };

  private findAncestorOfKind(node: Node | undefined, kinds: SyntaxKind[]): Node | null {
    for (; node != undefined; node = node.getParent()) {
      if (kinds.includes(node.getKind())) {
        return node;
      }
    }
    return null;
  }

  private parentIs(node: Identifier, parent: SyntaxKind[]): boolean {
    const kind: SyntaxKind | undefined = node.getParent()?.getKind();
    return parent.includes(kind);
  }

  visitStringLiteral(node: StringLiteral): void {
  }

  visitInterfaceDeclaration(node: InterfaceDeclaration): void {
    const element = new Element(Type.Class, node.getName());
    this.registerElement(node, element);
    this.push(element);
  }

  visitMethodSignature(node: MethodSignature): void {
    this.handleMethodLike(node.getName(), node);
  }

  visitClassDeclaration(node: ClassDeclaration): void {
    const isSourcefile = this.ancestorIsSourcefile(node.getParent());
    if (!isSourcefile) {
      return;
    }

    const element = new Element(Type.Class, node.getName() ?? "");
    this.registerElement(node, element);
    this.push(element);
  }

  visitConstructor(node: ConstructorDeclaration): void {
    const parentIsClass = this.currentElement().type === Type.Class;
    if (!parentIsClass) {
      return;
    }

    for (let parameter of node.getParameters()) {
      const isPrivate = parameter.hasModifier(SyntaxKind.PrivateKeyword);
      const isPublic = parameter.hasModifier(SyntaxKind.PublicKeyword);
      const isProtected = parameter.hasModifier(SyntaxKind.ProtectedKeyword);
      const isFieldDeclaration = isPrivate || isPublic || isProtected;

      if (isFieldDeclaration) {
        const element = new Element(Type.Field, parameter.getName());
        this.registerElement(parameter, element);
        this.push(element);
        this.pop();
      }
    }

    this.handleMethodLike("constructor", node);
  }

  visitGetAccessorDeclaration(node: GetAccessorDeclaration): void {
    this.handleMethodLike(node.getName(), node);
  }

  visitSetAccessorDeclaration(node: SetAccessorDeclaration): void {
    this.handleMethodLike(node.getName(), node);
  }

  visitMethodDeclaration(node: MethodDeclaration): void {
    this.handleMethodLike(node.getName(), node);
  }

  private handleMethodLike(name: string, node: Node): void {
    const parentIsClass = this.currentElement().type === Type.Class;
    if (!parentIsClass) {
      return;
    }

    const element = new Element(Type.Method, name);
    this.registerElement(node, element);
    this.push(element);
  }

  visitPropertyDeclaration(node: PropertyDeclaration): void {
    const parentIsClass = this.currentElement().type === Type.Class;
    if (!parentIsClass) {
      return;
    }

    const element = new Element(Type.Field, node.getName());
    this.registerElement(node, element);
    this.push(element);
  }

  visitFunctionDeclaration(node: FunctionDeclaration): void {
    const isSourcefile = this.ancestorIsSourcefile(node.getParent());
    if (!isSourcefile) {
      return;
    }

    const element = new Element(Type.Function, node.getName() ?? "");
    this.registerElement(node, element);
    this.push(element);
  }

  visitVariableDeclaration(node: VariableDeclaration): void {
    const isSourcefile = this.ancestorIsSourcefile(node.getParent());
    if (!isSourcefile) {
      return;
    }

    const element = new Element(Type.Variable, node.getName());
    this.registerElement(node, element);
    this.push(element);
  }

  private ancestorIsSourcefile(node: Node | undefined): boolean {
    for (; node != undefined; node = node.getParent()) {
      switch (node.getKind()) {
        case SyntaxKind.SourceFile:
          return true;
        case SyntaxKind.GetAccessor:
        case SyntaxKind.SetAccessor:
        case SyntaxKind.MethodDeclaration:
        case SyntaxKind.Constructor:
        case SyntaxKind.FunctionDeclaration:
        case SyntaxKind.FunctionExpression:
        case SyntaxKind.ArrowFunction:
          return false;
      }
    }
    console.log("unknown parent");
    return false;
  }

  visitTypeReference(node: TypeReferenceNode): void {
  }

  visitCallExpression(node: CallExpression): void {
  }

  visitPropertyAccessExpression(node: PropertyAccessExpression): void {
  }

  visitThisKeyword(node: ThisExpression): void {
  }

  visit(node: Node): void {
  }

  private registerElement(node: Node, element: Element): void {
    if (this.nodeToElement.has(node)) {
      throw new Error("element already registered: " + element.name);
    }
    this.nodeToElement.set(node, element);
  }

  private findElement(node: Node): Element | undefined {
    const element: Element | undefined = this.nodeToElement.get(node);
    return element;
  }

  private push(element: Element): void {
    let parent = this.currentElement();
    parent.children.push(element);
    this.elements.push(element);
  }

  private currentElement(): Element {
    return this.elements[this.elements.length - 1];
  }

  private pop(): void {
    this.elements.pop()!;
  }


  private addReference(from: Element, to: Node): void {
    const refs = this.references.get(from) ?? new Array();
    refs.push(to);
    this.references.set(from, refs);
  }

  private resolveReferences(): void {
    for (let from of this.references.keys()) {
      for (let toNode of this.references.get(from)!) {
        const to: Element | undefined = this.nodeToElement.get(toNode);
        if (!to) {
          continue;
        }

        from.references.push(to);
      }
    }
  }

  private addNonEmptyModuleCode(): void {
    for (let module of this.packageToModuleCode.keys()) {
      const moduleCode = this.packageToModuleCode.get(module);
      if (!moduleCode) {
        continue;
      }
      if (moduleCode.references.length != 0) {
        module.children.push(moduleCode);
      }
      if (moduleCode.children.length != 0) {
        throw new Error("expected no children in module code, got: " + moduleCode.children);
      }
    }
  }

}
