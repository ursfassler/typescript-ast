import {
  Node,
  SourceFile,
  Identifier,
  StringLiteral,
  InterfaceDeclaration,
  MethodSignature,
  FunctionDeclaration,
  MethodDeclaration,
  ClassDeclaration,
  ConstructorDeclaration,
  TypeReferenceNode,
  TypeNode,
  CallExpression,
  PropertyAccessExpression,
  ThisExpression,
  PropertyDeclaration,
  VariableDeclaration,
  GetAccessorDeclaration,
  SetAccessorDeclaration,
} from "ts-morph";
import { Visitor } from "../core/Visitor";


export class NodePrinter implements Visitor {
  private level: number = 0;
  private prefix: string = "";

  constructor(private writer: Function) {
  }

  begin(node: Node): void {
    this.level++;
    this.prefix = '  '.repeat(this.level);
  }

  end(node: Node): void {
    this.level--;
    this.prefix = '  '.repeat(this.level);
  }

  visitSourceFile(node: SourceFile): void {
    this.pr(node, node.getFilePath().toString());
  }

  visitIdentifier(node: Identifier): void {
    this.pr(node, node.getText());
  }

  visitStringLiteral(node: StringLiteral): void {
    this.pr(node, node.getText());
  }

  visitInterfaceDeclaration(node: InterfaceDeclaration): void {
    this.pr(node, node.getName());
  }

  visitMethodSignature(node: MethodSignature): void {
    this.pr(node, node.getName());
  }

  visitClassDeclaration(node: ClassDeclaration): void {
    this.pr(node, node.getName() ?? "--undefined--");
  }

  visitConstructor(node: ConstructorDeclaration): void {
    this.pr(node, "constructor");
  }

  visitGetAccessorDeclaration(node: GetAccessorDeclaration): void {
    this.pr(node, node.getName());
  }

  visitSetAccessorDeclaration(node: SetAccessorDeclaration): void {
    this.pr(node, node.getName());
  }

  visitMethodDeclaration(node: MethodDeclaration): void {
    this.pr(node, node.getName());
  }

  visitPropertyDeclaration(node: PropertyDeclaration): void {
    this.pr(node, node.getName());
  }

  visitFunctionDeclaration(node: FunctionDeclaration): void {
    const name: string = node.getName() ?? "--undefined--";
    this.pr(node, name);
  }

  visitVariableDeclaration(node: VariableDeclaration): void {
    this.pr(node, node.getName());
  }

  visitTypeReference(node: TypeReferenceNode): void {
    const args: TypeNode[] = node.getTypeArguments();
    const ref: string = args.map(x => x.getKindName()).join(", ");
    this.pr(node, ref);
  }

  visitCallExpression(node: CallExpression): void {
    this.pr(node, "");
  }

  visitPropertyAccessExpression(node: PropertyAccessExpression): void {
    this.pr(node, node.getName());
  }

  visitThisKeyword(node: ThisExpression): void {
    this.pr(node, "");
  }

  visit(node: Node): void {
    this.pr(node, "[" + node.getKind() + ", " + node.constructor.name + "]");
  }

  private pr(node: Node, value: string): void {
    this.writer(this.prefix + node.getKindName() + " " + value + "\n");
  }
}
