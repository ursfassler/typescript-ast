import { Project } from "ts-morph";
import { NodePrinter } from "./adapter/NodePrinter";
import { visit } from "./core/visit";
import fs from 'fs';
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import path from "path";
import { processProject } from "./processProject";


function main(argv: Array<string>) {
  const argParse = yargs(hideBin(argv))

  argParse.option('tsconfig', {
    demandOption: true,
    type: 'string',
    description: 'tsconfig.json file'
  });

  argParse.option('debug', {
    type: 'boolean',
    description: 'Enable debug mode'
  });

  const args = argParse.argv;
  const debug: boolean = args.debug as boolean;
  const tsConfigFilePath: string = args.tsconfig as string;

  console.info("load project")
  const project = new Project({
    tsConfigFilePath: tsConfigFilePath,
  });

  const sourceFiles = project.getSourceFiles();

  if (debug) {
    console.info("write debug output");
    const outFile: string = "debug.out";
    var ws = fs.createWriteStream(outFile);

    const defaultVisitor: NodePrinter = new NodePrinter(function (x: string) {
      ws.write(x);
    });
    for (let sourceFile of sourceFiles) {
      visit(sourceFile, defaultVisitor);
    }

    ws.close;
    console.info(`written to ${outFile}`);
  }

  const rootDirectory = path.dirname(tsConfigFilePath);

  const outFile: string = "out.ast";
  var ws = fs.createWriteStream(outFile);
  const writer = function (x: string): void {
    ws.write(x);
  };

  processProject(sourceFiles, rootDirectory, writer);

  ws.close;
  console.info(`written to ${outFile}`);
}


main(process.argv);
