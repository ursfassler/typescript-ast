import { Node } from "ts-morph";
import { dispatch, Visitor } from "./Visitor";

export function visit(node: Node, visitor: Visitor) {
  dispatch(node, visitor);
  visitor.begin(node);
  node.forEachChild(node => visit(node, visitor));
  visitor.end(node);
}
