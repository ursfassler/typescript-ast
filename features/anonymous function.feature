Feature: anonymous function


Scenario: ignore variables in anonymous functions
  Given I have the file "test.ts":
    """
    console.log((function() {
      const a: string = ".";
      return a;
    })());
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test"></package>
      </package>
    </project>
    """


Scenario: ignore variables in arrow functions
  Given I have the file "test.ts":
    """
    const a = () => {
      const b = 42;
    };
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable name="a"></variable>
        </package>
      </package>
    </project>
    """


Scenario: ignore functions in arrow functions
  Given I have the file "test.ts":
    """
    const a = 42;

    const b = () => {
      function foo(): number {
        return a;
      }
      return foo();
    };
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <variable name="b">
            <reference target="_1"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: ignore class in arrow functions
  Given I have the file "test.ts":
    """
    const a = 42;

    const b = () => {
      class A {
        public foo(): number {
          return a;
        }
      }
      return (new A()).foo();
    };
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <variable name="b">
            <reference target="_1"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: ignore fields of class in arrow functions
  Given I have the file "test.ts":
    """
    const x = () => {
      class A {
        private a: number;
        constructor(private b: number) {}
      }
    };
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable name="x"></variable>
        </package>
      </package>
    </project>
    """
