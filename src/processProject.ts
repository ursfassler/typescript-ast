import { SourceFile } from "ts-morph";
import { Element, Type } from "./core/Element";
import { ReducedAstPrinter } from "./core/ReducedAstPrinter";
import { visit } from "./core/visit";
import { SimpleWriter } from "./adapter/SimpleWriter";
import { writeProject } from "./core/writer";

export function processProject(sourceFiles: SourceFile[], rootDirectory: string, writer: (x: string) => void) {
    console.info("parse files");
    const root = parseFiles(sourceFiles, rootDirectory);
    console.info("write output");
    const sw: SimpleWriter = new SimpleWriter(writer);
    writeProject(root, sw);
}

export function parseFiles(sourceFiles: SourceFile[], rootDirectory: string): Element {
    let root = new Element(Type.Package, "");

    const visitor: ReducedAstPrinter = new ReducedAstPrinter(root, rootDirectory);
    visitor.beginProject();
    for (const sourceFile of sourceFiles) {
        console.info(sourceFile.getFilePath());
        visit(sourceFile, visitor);
    }
    visitor.endProject();

    return root;
}
