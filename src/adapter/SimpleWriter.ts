import { Writer } from "../core/writer";
const genx = require('genx');

export class SimpleWriter implements Writer {
  private xw = new genx.Writer(true, "\n", "  ");

  constructor(write: (x: string) => void) {
    this.xw.on('data', write);
  }

  startDocument(name: string): void {
    this.xw.startDocument();
    this.xw.startElementLiteral(name);
  }

  endDocument(): void {
    this.xw.endElement();
    this.xw.endDocument();
  }

  startElement(name: string): void {
    this.xw.startElementLiteral(name);
  }

  endElement(): void {
    this.xw.endElement();
  }

  attribute(name: string, value: string): void {
    this.xw.addAttributeLiteral(name, value);
  }

  toString(): string {
    return this.xw.toString();
  }
}
