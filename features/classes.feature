Feature: classes


Scenario: reference to other classes
  Given I have the file "test.ts":
    """
    class A {
    }

    class B {
    }

    class C {
      private x = new A();

      foo(y: B) {
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class id="_1" name="A"></class>
          <class id="_2" name="B"></class>
          <class name="C">
            <field name="x">
              <reference target="_1"></reference>
            </field>
            <method name="foo">
              <reference target="_2"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: reference to inherited class
  Given I have the file "test.ts":
    """
    class A {
    }

    class B extends A {
    }

    class C extends B {
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class id="_1" name="A"></class>
          <class id="_2" name="B">
            <reference target="_1"></reference>
          </class>
          <class name="C">
            <reference target="_2"></reference>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: reference to interface
  Given I have the file "test.ts":
    """
    interface A {
    }

    interface B {
    }

    class C implements A, B {
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class id="_1" name="A"></class>
          <class id="_2" name="B"></class>
          <class name="C">
            <reference target="_1"></reference>
            <reference target="_2"></reference>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: reference properties from methods
  Given I have the file "test.ts":
    """
    class Test {
      private a = 42;
      private b = "hello";

      foo() {
        return this.a;
      }

      bar(x) {
        this.b = x;
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <field id="_1" name="a"></field>
            <field id="_2" name="b"></field>
            <method name="foo">
              <reference target="_1"></reference>
            </method>
            <method name="bar">
              <reference target="_2"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: reference property multiple times from method
  Given I have the file "test.ts":
    """
    class Test {
      private a: number = 42;
      private b: string = "";

      foo(x: number): string {
        while (this.a < x) {
          this.b += ".";
          this.a = this.a + 2;
        }
        return this.b;
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <field id="_1" name="a"></field>
            <field id="_2" name="b"></field>
            <method name="foo">
              <reference target="_1"></reference>
              <reference target="_2"></reference>
              <reference target="_1"></reference>
              <reference target="_1"></reference>
              <reference target="_2"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: initialize properties from constructor
  Given I have the file "test.ts":
    """
    class Test {
      private a;
      private b;

      constructor() {
        this.a = 42;
        this.b = "hello";
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <field id="_1" name="a"></field>
            <field id="_2" name="b"></field>
            <method name="constructor">
              <reference target="_1"></reference>
              <reference target="_2"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: declare property in constructor
  Given I have the file "test.ts":
    """
    class Test {
      constructor(private a; protected b; public c; d) {
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <field id="_1" name="a"></field>
            <field id="_2" name="b"></field>
            <field id="_3" name="c"></field>
            <method name="constructor">
              <reference target="_1"></reference>
              <reference target="_2"></reference>
              <reference target="_3"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: class property getter
  Given I have the file "test.ts":
    """
    const y = 42;

    class A {
      get foo(): number {
        return y;
      }
    };

    const x = (new A()).foo;
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="y"></variable>
          <class id="_2" name="A">
            <method id="_3" name="foo">
              <reference target="_1"></reference>
            </method>
          </class>
          <variable name="x">
            <reference target="_2"></reference>
            <reference target="_3"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: class property setter
  Given I have the file "test.ts":
    """
    let y = 0;

    class A {
      set foo(value: number) {
        y = value;
      }
    };

    (new A()).foo = 42;
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="y"></variable>
          <class id="_2" name="A">
            <method id="_3" name="foo">
              <reference target="_1"></reference>
            </method>
          </class>
          <function name="__init__">
            <reference target="_2"></reference>
            <reference target="_3"></reference>
          </function>
        </package>
      </package>
    </project>
    """


Scenario: reference variable from variable declaration in getter and setter
  Given I have the file "test.ts":
    """
    let a = 0;

    class A {
      get foo(): number {
        const x = a;
        return x;
      }

      set bar(value: number) {
        const x = value;
        a = value;
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <class name="A">
            <method name="foo">
              <reference target="_1"></reference>
            </method>
            <method name="bar">
              <reference target="_1"></reference>
            </method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: method signature in method declaration
  Given I have the file "test.ts":
    """
    declare class A {
      foo(): {
        bar(): void;
      };
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="A">
            <method name="foo"></method>
          </class>
        </package>
      </package>
    </project>
    """
