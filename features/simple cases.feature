Feature: simple cases


Scenario: Empty class
  Given I have the file "test.ts":
    """
    class Test {
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test"></class>
        </package>
      </package>
    </project>
    """


Scenario: Class with methods
  Given I have the file "test.ts":
    """
    class Test {
      a() {
      }

      b() {
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <method name="a"></method>
            <method name="b"></method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: Class with properties
  Given I have the file "test.ts":
    """
    class Test {
      private a = 42;
      private b = "hello world";
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <field name="a"></field>
            <field name="b"></field>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: Class in namespace
  Given I have the file "a/b/test.ts":
    """
    class Test {
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="a">
          <package name="b">
            <package name="test">
              <class name="Test"></class>
            </package>
          </package>
        </package>
      </package>
    </project>
    """


Scenario: simple function
  Given I have the file "test.ts":
    """
    function foo() {
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <function name="foo"></function>
        </package>
      </package>
    </project>
    """


Scenario: simple variable
  Given I have the file "test.ts":
    """
    const a = "Hello world";
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable name="a"></variable>
        </package>
      </package>
    </project>
    """
