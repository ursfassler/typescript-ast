import { expect } from "chai";
import { Given, When, Then, Before } from "@cucumber/cucumber";
import { FileSystemHost, Project } from "ts-morph";
import { parseFiles } from "../processProject";
import { SimpleWriter } from "../adapter/SimpleWriter";
import { writeProject } from "../core/writer";


Before(function () {
    let project = new Project({
        useInMemoryFileSystem: true
    });
    let fs: FileSystemHost = project.getFileSystem();
    fs.writeFileSync("/app/tsconfig.json", "{}");
    this.project = project;
    this.fs = fs;
})

Given('I have the file {string}:', function (filename: string, content: string) {
    this.fs.writeFileSync("/app/" + filename, content);
});

When('I parse the project', function () {
    this.project.addSourceFilesFromTsConfig("/app/tsconfig.json");

    const rootDirectory = "/app/";
    const project = this.project;

    this.ast = parseFiles(project.getSourceFiles(), rootDirectory);
});

Then('I expect to have the AST:', function (expected: string) {
    let actual: string = "";
    const writer = function (x: string): void {
        actual += x;
    };

    const sw: SimpleWriter = new SimpleWriter(writer);
    writeProject(this.ast, sw);

    expect(actual).to.equal(expected);
});
