Feature: variables


Scenario: parse variables with references
  Given I have the file "test.ts":
    """
    const a = "hello";
    const b = "world";
    let c = a + " " + b;
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <variable id="_2" name="b"></variable>
          <variable name="c">
            <reference target="_1"></reference>
            <reference target="_2"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: parse variables with reference to other module
  Given I have the file "a.ts":
    """
    export const A = 21;
    """
  Given I have the file "b.ts":
    """
    import { A } from "./a";

    const B = A * 2;
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="a">
          <variable id="_1" name="A"></variable>
        </package>
        <package name="b">
          <variable name="B">
            <reference target="_1"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: ignore variables in methods
  Given I have the file "test.ts":
    """
    class Test {
      foo(): void {
        const a: string = ".";
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <method name="foo"></method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: ignore variables in constructor
  Given I have the file "test.ts":
    """
    class Test {
      constructor() {
        const a: string = ".";
      }
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <class name="Test">
            <method name="constructor"></method>
          </class>
        </package>
      </package>
    </project>
    """


Scenario: ignore variables in functions
  Given I have the file "test.ts":
    """
    function foo() {
      const a: string = ".";
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <function name="foo"></function>
        </package>
      </package>
    </project>
    """


Scenario: reference variable from variable declaration in function
  Given I have the file "test.ts":
    """
    const y = 42;

    function foo(): number {
      const a = y;
      return a;
    }
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="y"></variable>
          <function name="foo">
            <reference target="_1"></reference>
          </function>
        </package>
      </package>
    </project>
    """
