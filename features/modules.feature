Feature: modules


Scenario: parse module code
  Given I have the file "test.ts":
    """
    const a = "hello";
    const b = "world";

    console.log(a + " " + b);
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <variable id="_2" name="b"></variable>
          <function name="__init__">
            <reference target="_1"></reference>
            <reference target="_2"></reference>
          </function>
        </package>
      </package>
    </project>
    """


Scenario: namespace import from module that has imports
  Given I have the file "a.ts":
    """
    import * as ab from './b';
    """
  And I have the file "b.ts":
    """
    import { cc } from "./c"
    """
  And I have the file "c.ts":
    """
    const cc = 0;
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="a"></package>
        <package name="b"></package>
        <package name="c">
          <variable name="cc"></variable>
        </package>
      </package>
    </project>
    """
