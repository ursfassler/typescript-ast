Feature: object literal


Scenario: add reference from object literal method
  Given I have the file "test.ts":
    """
    const a = 42;

    const b = {
      foo(): number {
        return a;
      },
    };
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <variable id="_1" name="a"></variable>
          <variable name="b">
            <reference target="_1"></reference>
          </variable>
        </package>
      </package>
    </project>
    """


Scenario: add reference from object literal field
  Given I have the file "test.ts":
    """
    function foo(x: any): void {
    }

    const a = 42;

    foo({
      b: a,
    });
    """

  When I parse the project

  Then I expect to have the AST:
    """
    <project language="TypeScript">
      <package name="">
        <package name="test">
          <function id="_1" name="foo"></function>
          <variable id="_2" name="a"></variable>
          <function name="__init__">
            <reference target="_1"></reference>
            <reference target="_2"></reference>
          </function>
        </package>
      </package>
    </project>
    """
