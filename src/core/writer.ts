import { Element, Type } from "./Element";


export interface Writer {
  startDocument(name: string): void;
  endDocument(): void;
  startElement(name: string): void;
  endElement(): void;
  attribute(name: string, value: string): void;
}


export function writeProject(root: Element, wr: Writer): void {
  const ids: IdGenerator = new IdGenerator(root);

  wr.startDocument("project");
  wr.attribute("language", "TypeScript");
  writeElement(root, ids, wr);
  wr.endDocument();
}

function writeElement(element: Element, ids: IdGenerator, wr: Writer): void {
  wr.startElement(typeName(element.type));
  wr.attribute("name", element.name);

  const id = ids.getId(element);
  if (id) {
    wr.attribute("id", id);
  }

  for (const ref of element.references) {
    const targetId = ids.getId(ref);

    if (!targetId) {
      console.warn(`target id not found for "${ref.name}", referenced from "${element.name}"`);
      continue;
    }

    wr.startElement("reference");
    wr.attribute("target", targetId);
    wr.endElement();
  }

  for (const child of element.children) {
    writeElement(child, ids, wr);
  }

  wr.endElement();
}

function typeName(value: Type): string {
  switch (value) {
    case Type.Class: return "class";
    case Type.Field: return "field";
    case Type.Function: return "function";
    case Type.Method: return "method";
    case Type.Package: return "package";
    case Type.Variable: return "variable";
  }
  throw Error(`undefined type: ${value}`);
}

class IdGenerator {
  private referenced: Set<Element> = new Set();
  private ids: Map<Element, string> = new Map();
  private lastId: number = 0;

  constructor(root: Element) {
    this.gatherReferencedElements(root);
    this.assignIds(root);
  }

  private gatherReferencedElements(element: Element): void {
    for (let ref of element.references) {
      this.referenced.add(ref);
    }
    for (let child of element.children) {
      this.gatherReferencedElements(child);
    }
  }

  private assignIds(element: Element): void {
    if (this.referenced.has(element)) {
      this.lastId++;
      const id = '_' + this.lastId.toString();
      this.ids.set(element, id);
    }
    for (let child of element.children) {
      this.assignIds(child);
    }
  }

  public getId(element: Element): string | undefined {
    return this.ids.get(element);
  }

}