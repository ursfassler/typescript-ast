
export enum Type {
  Package,
  Variable,
  Function,
  Class,
  Field,
  Method,
}

export class Element {
  children: Array<Element> = [];
  references: Array<Element> = [];

  constructor(public type: Type, public name: string) {
  }

}
